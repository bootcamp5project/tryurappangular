import { TryUrAppPage } from './app.po';

describe('try-ur-app App', () => {
  let page: TryUrAppPage;

  beforeEach(() => {
    page = new TryUrAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
