# Cliente Angular

## Setup

Fork del proyecto

npm install

ng serve


## Documentación de interés

Simple pero completo curso paso a paso para tener una visión global de las herramientas:

* [CodeCraft Angular](https://codecraft.tv/courses/angular/quickstart/overview/)
* [Angular.IO quickstart](https://angular.io/guide/quickstart)

Articulos de interés:

* [Estructurar un proyecto angular](https://medium.com/@motcowley/angular-folder-structure-d1809be95542)
 



## Estructura del proyecto

```
├── app
│   ├── app.component.css
│   ├── app.component.html
│   ├── app.component.spec.ts
│   ├── app.component.ts
│   ├── app.module.ts
│   ├── app.routing.ts
│   ├── dashboard
│   │   ├── dashboard.component.css
│   │   ├── dashboard.component.html
│   │   ├── dashboard.component.spec.ts
│   │   └── dashboard.component.ts
│   ├── login
│   │   ├── login.component.css
│   │   ├── login.component.html
│   │   ├── login.component.spec.ts
│   │   └── login.component.ts
│   ├── register
│   │   ├── register.component.css
│   │   ├── register.component.html
│   │   ├── register.component.spec.ts
│   │   └── register.component.ts
│   ├── services
│   │   ├── storage.service.spec.ts
│   │   ├── storage.service.ts
│   │   └── user.service.ts
│   └── shared
│       ├── authorizated.guard.ts
│       ├── fake-backend.ts
│       ├── mock-users.ts
│       └── models
│           ├── login-object.model.ts
│           ├── session.model.ts
│           └── user.model.ts
├── assets
├── environments
│   ├── environment.prod.ts
│   └── environment.ts
├── favicon.ico
├── index.html
├── main.ts
├── polyfills.ts
├── styles.css
├── test.ts
├── tsconfig.app.json
├── tsconfig.spec.json
└── typings.d.ts
```


## Descripción de los elementos

## Histórico

20/03/2018. Login y registro de usuarios.



## Problemas encontrados


# Dockerized Angular 4 App (with Angular CLI)
- url: https://github.com/avatsaev/angular4-docker-example

## Build docker image

```
$ docker-compose build

```
servidor
```
$ docker build -t myapp . 
```

## Run the container

```
local
$ sudo docker run -ti --rm -v /home/abgl/Escritorio/FastApp/tryurappangular/:/usr/src/app -p4200:4200 -p49152:49152 tryurappangular_client-angular bash
```
servidor
```
docker run --name webapp -ti --net dockerwp --rm -p 8080:80 -e VIRTUAL_HOST=tryurapp.com,www.tryurapp.com -e LETSENCRYPT_HOST=tryurapp.com,www.tryurapp.com -e LETSENCRYPT_EMAIL=bootcamp5_project@gmail.com -d myapp

```

## Run the webapp
```
npm install

ng serve --host 0.0.0.0 --poll=2000
```


The app will be available at http://localhost:4200


