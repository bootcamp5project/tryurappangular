import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { MatButtonModule, MatToolbarModule } from '@angular/material';
import { LoginComponent } from '../login/login.component';
import { StorageService } from "../services/storage.service";
import { Subscription } from 'rxjs/Subscription';
import { EmptyError } from 'rxjs';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    viewLogin: boolean = true
    viewLoginRegisterLayer: boolean = false;
    isAuthenticated: boolean = false;


    constructor(private storageService: StorageService, 
        private userService: UserService, private router: Router) {
        // Start.
        storageService.isAuthenticated()
        
        // If Changes Authentication
        storageService.userIsAuthenticated$.subscribe(
            isAuthenticated => {
                this.isAuthenticated = isAuthenticated  
            }
        )
        userService.userViewLoginRegisterLayerOnChange$.subscribe(
            viewLoginRegister => {
                this.viewLoginRegisterLayer = viewLoginRegister
            }
        )
    }

    ngOnInit() {
    }

    toggleLoginRegister() {
        this.viewLogin = !this.viewLogin
    }

    toggleViewLoginRegisterLayer() {
        this.userService.loginRegisterViewLayer(!this.viewLoginRegisterLayer)
    }

    loginOut() {
        this.storageService.logout();
    }

    // Puede ver el login o el registo segun la view que estés
    // pasando.
    signUpView(view: string) {
        this.userService.loginRegisterViewLayer(true)
        this.viewLogin = view === 'login'?true:false
        this.viewLoginRegisterLayer = true
    }

    signUpViewClose() {
        this.userService.loginRegisterViewLayer(false)
    }

}
