import { Component, OnInit, Inject } from '@angular/core';
import { User } from '../shared/models/user.model'
import { UserService } from "../services/user.service";
import { Router } from "@angular/router";
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from "@angular/material/snack-bar"

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { StorageService } from '../services/storage.service';



@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

    public submitted: Boolean = false;
    public editing: boolean = false;
    public error: { code: number, message: string } = null;

    public snackBarRef: MatSnackBarRef<SimpleSnackBar>

    // This user comes from userService. First Empty
    public user: User

    // User model: It's binding to form every you write in form is in this model. userModel <----> editableForm
    public userModel: User = new User();

    
    // Hide and show password
    hide = true;
    
    //Dialog Ref
    dialogRef: MatDialogRef<ConfirmationDialogComponent>;
    
    constructor(private userService: UserService, private storageService: StorageService, private router: Router, public snackBar: MatSnackBar,
        public dialog: MatDialog) {
        }
        
        ngOnInit() {
            this.setUpFormWithUserFromService()
            
        }

    public modelOnChange() {
        console.log("model on change")
        if (this.editing === false) {
            console.log("editing is false....")

            // We are in editing mode show buttoms
            this.editing = true
            this.snackBarRef = this.snackBar.open('Edition Mode...');
        }

    }
    public onSubmit(): void {
        this.submitted = true;
        this.error = null;

        this.userService.updateUser(this.userModel.name, this.userModel.email, this.storageService.getCurrentToken()).subscribe(
            data => {
                switch (data.status) {
                    case 200:
                        // Sync user with model
                        this.user.name = this.userModel.name
                        this.user.email = this.userModel.email
                        this.user.password = this.userModel.password

                        // Editing mode done
                        this.editing = false
                        this.snackBarRef.dismiss()
                        break;
                }
            },
            error => {
                this.error = {
                    code: 500,
                    message: "Some validation error"
                }

            })

    }

    // Map user that come from service with form view
    private setUpFormWithUserFromService() {
        // New user
        this.user = new User()
        // This data is collected when user login process
        let email = this.storageService.getCurrentUser().email;
        let token = this.storageService.getCurrentToken()

        this.userService.getUserInfo(email, token).subscribe(
            data => {
                console.log("Datos de usuario" + JSON.stringify(data.result.name))
                // Suponemos un 200  
                this.user.name = data.result.name
                this.user.email = data.result.email
                this.user.password = data.result.password

                //Update Model
                this.userModel.name = this.user.name
                this.userModel.email = this.user.email
                this.userModel.password = this.user.password
            },
            error => {
                console.log("Error al traer datos de de usuario")
            }
        )

    }

    private cancelEdition() {
        this.editing = false
        this.snackBarRef.dismiss()
        this.userModel.name = this.user.name
        this.userModel.email = this.user.email
        this.userModel.password = this.user.password
    }

    openDialog(): void {
        this.dialogRef = this.dialog.open(ConfirmationDialogComponent);

        this.dialogRef.afterClosed().subscribe(
            answer => {
                console.log("Esta derrado el dialog" + answer)
                if(answer==='Yes'){
                    this.userService.deleteUser(this.user.email, this.storageService.getCurrentToken()).subscribe(
                        data => {
                            switch(data.status){
                                case 204:
                                    this.storageService.logout()
                                break;
                            }
                        }
                    )
                }
            }
        )
    }

}