import { Component } from '@angular/core';
import { StorageService } from "../services/storage.service";
import { UserService } from "../services/user.service";
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconRegistry } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { User } from "../shared/models/user.model";


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent {

  public user: User;
  display = 'none'
  viewLogin: Boolean = true

  constructor(
    private storageService: StorageService,
    private UserService: UserService,
    iconRegistry: MatIconRegistry
  ) { }

  ngOnInit() {
    this.user = this.storageService.getCurrentUser();
  }

  public logout(): void {
    this.UserService.logout().subscribe(
      response => { if (response) { this.storageService.logout(); } }
    );
  }

  public openModal(openWith: String) {
    this.display = "block";
    if (openWith === "login") {
      this.viewLogin = true
    }
    else {
      this.viewLogin = false
    }
  }

  onCloseHandled() {
    this.display = 'none';
  }

  changeViewModal() {
    console.log("Change view modal")
    this.viewLogin = !this.viewLogin
  }

}
