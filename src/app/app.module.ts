import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

// Material
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatSelectModule,
  MatStepperModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatToolbarModule,
  MatGridListModule,
  MatSnackBar,
  MatSnackBarModule,
  MatSnackBarRef
} from '@angular/material';

import { AppComponent } from './app.component';
import {Routing} from "./app.routing";
import { LoginComponent } from './login/login.component';
import { LandingComponent } from './landing/landing.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

// Services
import { StorageService } from './services/storage.service';
import { UserService } from './services/user.service'

// Test without backend
import {MockBackend} from "@angular/http/testing";
import {fakeBackendProvider} from "./shared/fake-backend";

import {BaseRequestOptions} from "@angular/http";
import {AuthorizatedGuard} from "./shared/guards/authorizated.guard";
import { LandingGuard } from './shared/guards/landing.guard';

// Social Login
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider} from "angularx-social-login";
import { SettingsComponent } from './settings/settings.component';
import { SectionsNavBarComponent } from './sections-nav-bar/sections-nav-bar.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';

// Social Login SetUp
let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("1076721637217-e3ar667avgtj57f5rvmi0icqk9bg2okl.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("160371631456230")  

  }
]);



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LandingComponent,
    DashboardComponent,
    RegisterComponent,
    HeaderComponent,
    FooterComponent,
    SettingsComponent,
    SectionsNavBarComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    Routing,
    ReactiveFormsModule,

    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatToolbarModule,
    MatStepperModule,
    MatGridListModule,
    BrowserAnimationsModule,
    FlexLayoutModule,

    SocialLoginModule.initialize(config)
  ],
  providers: [
    StorageService,
    UserService,
    AuthorizatedGuard,
    LandingGuard,
//    fakeBackendProvider,
//    MockBackend,
    BaseRequestOptions
  ],
  // dialog we need
  entryComponents: [ConfirmationDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
