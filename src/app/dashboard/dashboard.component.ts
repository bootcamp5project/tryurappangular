import { Component, isDevMode } from '@angular/core';
import { StorageService } from "../services/storage.service";
import { User } from "../shared/models/user.model";
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconRegistry } from '@angular/material';
import { UserService } from "../services/user.service";

import { AuthService, SocialUser } from "angularx-social-login";
import { Router } from '@angular/router';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

    public user: User;

    constructor(
        private storageService: StorageService,
        private userService: UserService,
        private router: Router
    ) { }

    ngOnInit() {
        this.user = new User()
        // This data is collected when user login process
        let email = this.storageService.getCurrentUser().email;
        let token = this.storageService.getCurrentToken()

        this.userService.getUserInfo(email, token).subscribe(
            data => {
                console.log("Datos de usuario" + JSON.stringify(data))
                this.processResponse(data)
            },
            error => {
                console.log("Error al traer datos de de usuario")
            }
        )

    }

    public logout(): void {
        this.storageService.logout();
    }

    public editUserInfo() {
        this.router.navigate(['/settings']);
    }

    private processResponse(data: any) {
        switch (data.status) {
            case 200:
                this.user.name = data.result.name
                this.user.email = data.result.email
                break;

            default:
                if (isDevMode()) {
                    console.log("Otro error")
                }
        }
    }

}
