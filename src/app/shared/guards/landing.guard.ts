import { Injectable, isDevMode } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {StorageService} from '../../services/storage.service'

@Injectable()
export class LandingGuard implements CanActivate {

  constructor(private router: Router,
              private storageService: StorageService) { }

  canActivate() {
    if (isDevMode()) {
    console.log(this.storageService.isAuthenticated());
    }
    if (this.storageService.isAuthenticated()) {
      // Logged in so redirect to login page
      this.router.navigate(['/dashboard']);
      return false;
    }
    
    // Not logged in so return true
    return true;
  }
}