import { Injectable, isDevMode } from "@angular/core";
import { Http, Response, Headers, RequestOptions, ResponseOptions } from "@angular/http";
import { Observable } from "rxjs";
import { LoginObject } from "../shared/models/login-object.model";
import { Session } from "../shared/models/session.model";
import { User } from "../shared/models/user.model"
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map'
import { Subject } from 'rxjs/Subject';

@Injectable()
export class UserService {

    //data source
    private userViewLoginRegisterLayerOnChangeSource = new Subject<boolean>();

    //Observable
    public userViewLoginRegisterLayerOnChange$ = this.userViewLoginRegisterLayerOnChangeSource.asObservable();

    constructor(private http: Http) { }

    private basePath: string = environment.apiUrl;

    login(loginObj: LoginObject): Observable<Boolean> {

        let myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');

       /* let body = new URLSearchParams();
        body.append("email", loginObj.email);
        body.append("password", loginObj.password);

        if (isDevMode()) {
            console.log("loginObj" + JSON.stringify(loginObj))
            console.log(body.toString())
        }

        let options = new RequestOptions({ headers: myHeaders });*/

        return this.http.get(
            this.basePath + "authenticate" +
            "?email=" +
            loginObj.email
            + "&password=" +
            loginObj.password
        ).map(this.extractData);

    }


    logout(): Observable<Boolean> {
        return this.http.post(this.basePath + 'logout', {}).map(this.extractData);
    }

    register(user: User): Observable<any> {
        let myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');

        let body = new URLSearchParams();
        body.append("name", user.name)
        body.append("email", user.email);
        body.append("password", user.password);

        let options = new RequestOptions({ headers: myHeaders });

        return this.http.post(
            this.basePath + 'register',
            body.toString(), options
        ).map(this.extractData);
    }

    loginRegisterViewLayer(viewlayer: boolean) {
        this.userViewLoginRegisterLayerOnChangeSource.next(viewlayer);
    }

    getUserInfo(email: string, token: string): Observable<any> {
        return this.http.get(
            this.basePath +
            "?email=" +
            email
            + "&token=" +
            token
        ).map(this.extractData);

    }

    updateUser(name: string, email: string, token: string):Observable<any> {

        let myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');

        let body = new URLSearchParams();
        body.append("name", name);
        body.append("email", email);
        body.append("token", token);

        let options = new RequestOptions({ headers: myHeaders });

        return this.http.post(
            this.basePath + 'update',
            body.toString(), options
        ).map(this.extractData);

    }

    deleteUser(email: string, token: string):Observable<any> {

        let myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');

        let body = new URLSearchParams();
        body.append("email", email);
        body.append("token", token);

        let options = new RequestOptions({ headers: myHeaders });

        return this.http.post(
            this.basePath + 'delete',
            body.toString(), options
        ).map(this.extractData);

    }

    private extractData(res: Response) {
        if (isDevMode()) {
            console.log("RESPONSE" + JSON.stringify(res))
        }
        let body = res.json();
        if (isDevMode()) {
            console.log("Este es el body: " + body);
        }
        return body;
    }

}