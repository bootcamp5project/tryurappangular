import { Injectable } from "@angular/core";
import { Router } from '@angular/router';
import { Session } from "../shared/models/session.model";
import { User } from "../shared/models/user.model";

import { Subject }    from 'rxjs/Subject';


@Injectable()
export class StorageService {

    private localStorageService;
    private currentSession: Session = null;

    //data source
    private userIsAuthenticatedSource = new Subject<boolean>();

    // Observable  sources
    public userIsAuthenticated$ = this.userIsAuthenticatedSource.asObservable()


    constructor(private router: Router) {
        this.localStorageService = localStorage;
        this.currentSession = this.loadSessionData();
    }

    setCurrentSession(session: Session): void {
        this.currentSession = session;
        this.localStorageService.setItem('currentUser', JSON.stringify(session));
    }

    loadSessionData(): Session {
        var sessionStr = this.localStorageService.getItem('currentUser');
        return (sessionStr) ? <Session>JSON.parse(sessionStr) : null;
    }

    getCurrentSession(): Session {
        return this.currentSession;
    }

    removeCurrentSession(): void {
        this.localStorageService.removeItem('currentUser');
        this.currentSession = null;
    }

    getCurrentUser(): User {
        var session: Session = this.getCurrentSession();
        return (session && session.user) ? session.user : null;
    };

    isAuthenticated(): boolean {
        let a = (this.getCurrentToken() != null) ? true : false;
        this.userIsAuthenticatedSource.next(a);
        return a;
    };

    getCurrentToken(): string {
        var session = this.getCurrentSession();
        return (session && session.token) ? session.token : null;
    };

    logout(): void {
        this.removeCurrentSession();
        this.router.navigate(['/login']);
    }

}
