import { Component, isDevMode } from "@angular/core";
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { LoginObject } from "../shared/models/login-object.model";
import { UserService } from "../services/user.service";
import { StorageService } from "../services/storage.service";
import { Router } from "@angular/router";
import { Session } from "../shared/models/session.model";
import { User } from "../shared/models/user.model"
import { Output, EventEmitter } from '@angular/core';

import { AuthService, SocialUser } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})

export class LoginComponent {
    public loginForm: FormGroup;
    public submitted: Boolean = false;
    public error: { code: number, message: string } = null;



    @Output()
    registerClick: EventEmitter<void> = new EventEmitter<void>()

    constructor(private formBuilder: FormBuilder,
        private userService: UserService,
        private storageService: StorageService,
        private router: Router,
        private authService: AuthService) { }

    ngOnInit() {
        this.initializeForm()
    }

    public submitLogin(): void {
        let user: User = new User()
        let session: Session = new Session()
        //this.loginAction(user, session)

        user.email = this.loginForm.value.email
        session.user = user
        this.submitted = true;
        this.error = null;
        if (this.loginForm.valid) {
           this.loginAction(session, new LoginObject(this.loginForm.value))
        }
    }

    public loginAction(session: Session,loginForm: LoginObject): void {
        this.userService.login(loginForm).subscribe(
            data => {
                this.processResponse(data, session)
            },
            error => {
                this.error = {
                    code: 500,
                    message: "Connection Error"
                }
                this.initializeForm()
            }
        )
        
    }


    public goRegister(event) {
        this.registerClick.emit()
    }

    private correctLogin(data: Session) {
        this.userService.loginRegisterViewLayer(false)
        this.storageService.isAuthenticated()
        this.storageService.setCurrentSession(data)
        this.router.navigate(['/dashboard']);

    }

    private initializeForm() {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.pattern("[^ @]*@[^ @]*"),
            Validators.required]
            ],
            password: ['', [Validators.minLength(3),
            Validators.required]],
        })

    }

    signInWithGoogle(): void {
        this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
            .then(
                (socialUser) => {
                    this.saveSocialUser(socialUser)
                }
            );
    }

    signInWithFacebook(): void {
        this.authService.signIn(FacebookLoginProvider.PROVIDER_ID)
            .then(
                (socialUser) => {
                    this.saveSocialUser(socialUser)
                }
            )
    };

    signOut(): void {
        if (isDevMode()) {
            console.log("Emito el evento en signout")
        }
        this.authService.signOut();
    }

    private saveSocialUser(socialUser: SocialUser) {
        if (socialUser !== null) {
            if (isDevMode()) {
                console.log("OK, register and if ok local storage")
            }
            // Password social user. Pendiente de cambiar API
            let socialUserPassword = "socialUserPassword"

            let session: Session
            let user: User = {
                id: 1,
                name: socialUser.name,
                surname: socialUser.lastName,
                email: socialUser.email,
                username: socialUser.lastName,
                password: socialUserPassword
            }
            session = {
                token: socialUser.authToken,
                user: user
            }

            // If user doesn't exists register
            // Register  Request
            // If User exists Update Token
            // TODO: Nuevos Endpoints
            // User exists - email
            // updateToken - email, oldToken, newToken
            //this.correctLogin(session)

            //register & login
            let loginObject = {"email": `${socialUser.email}`,
                                "password": `${socialUserPassword}` }


            this.userService.register(user).subscribe(
                data => {
                    switch (data.status) {
                        case 200:
                            //Registramos el social user en nuestra API ok--> login
                            this.loginAction(session, new LoginObject(loginObject) )
                            break;
                        case 403:
                            // El usuario social ya está registrado --> login
                            this.loginAction(session, new LoginObject(loginObject))
                            this.error = {
                                code: 403,
                                message: data.result
                            }
                            break;
                    }

                },
                error => {
                    this.error = {
                        code: 500,
                        message: "Some validation error"
                    }
                })
        }
    }

    private processResponse(data: any, session: Session) {
        if (isDevMode()) {
            console.log("Recibido de backend:" + JSON.stringify(data))
            console.log("session: " + session)
        }

        switch (data.status) {
            case 200:
                session.token = data.resultDescription
                this.correctLogin(session)

                break;
            case 403:
                this.error = {
                    code: 403,
                    message: data.result
                }
                break;
            case 404:
                this.error = {
                    code: 404,
                    message: data.result
                }
                break;

            default:
                if (isDevMode()) {
                    console.log("Otro error")
                }

        }

    }



}
