import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from "./services/user.service";
import { StorageService } from './services/storage.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [UserService]
})
export class AppComponent {
    overflowy = 'auto';

    constructor(private router: Router, private userService: UserService, private storageService: StorageService) {
        userService.userViewLoginRegisterLayerOnChange$.subscribe(
            () => {
                this.overflowy = (this.overflowy === 'auto')?'hidden':'auto'
            }
        )

    }
}

