import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {SettingsComponent} from "./settings/settings.component";
import {LoginComponent} from "./login/login.component";
import {LandingComponent} from "./landing/landing.component";
import {AuthorizatedGuard} from "./shared/guards/authorizated.guard";
import {LandingGuard} from "./shared/guards/landing.guard";
import {RegisterComponent} from './register/register.component'

const appRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent, canActivate: [ AuthorizatedGuard ]},
  { path: 'settings', component: SettingsComponent, canActivate: [ AuthorizatedGuard ]},
  { path: 'landing', component: LandingComponent, canActivate: [ LandingGuard]},
 // { path: 'login', component: LoginComponent },
 // { path: 'register', component: RegisterComponent },
 // { path: '', redirectTo: '/landing', pathMatch: 'full' },
  { path: '**', redirectTo: '/landing'}
];

export const Routing = RouterModule.forRoot(appRoutes);