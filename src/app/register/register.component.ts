import { Component, OnInit } from '@angular/core';
import { User } from '../shared/models/user.model'
import { UserService } from "../services/user.service";
import { Router } from "@angular/router";
import { EventEmitter, Output } from '@angular/core';


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent {

    model: User = new User()
    submitted = false;
    error: { code: number, message: string } = null;

    @Output()
    loginClick: EventEmitter<void> = new EventEmitter<void>()

    constructor(private userService: UserService,
        private router: Router) { }


    ngOnInit() {
        
    }

    onSubmit() {
        this.submitted = true
        this.userService.register(this.model).subscribe(
            data => this.goLogin(),
            error => {
                this.error = {
                    code: 500,
                    message: "Some validation error"
                }
                // Restart model
                this.model = new User()
                this.submitted = false;
            })
    }

    public goLogin() {
        this.loginClick.emit()
    }

}

