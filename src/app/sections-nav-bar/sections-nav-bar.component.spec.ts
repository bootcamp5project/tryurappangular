import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionsNavBarComponent } from './sections-nav-bar.component';

describe('SectionsNavBarComponent', () => {
  let component: SectionsNavBarComponent;
  let fixture: ComponentFixture<SectionsNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionsNavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionsNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
