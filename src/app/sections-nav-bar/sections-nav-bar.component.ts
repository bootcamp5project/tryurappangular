import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sections-nav-bar',
  templateUrl: './sections-nav-bar.component.html',
  styleUrls: ['./sections-nav-bar.component.css']
})
export class SectionsNavBarComponent implements OnInit {

  sections = [
    { link: 'dashboard', label: 'Dashboard' },
    { link: 'settings', label: 'Settings' },
  ];

  constructor() { }

  ngOnInit() {
  }

}
